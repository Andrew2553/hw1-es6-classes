// 1. Прототипне наслідування дозволяє створювати ієрархії об'єктів, де деякі об'єкти можуть спільно використовувати властивості та методи батьківських об'єктів. Це робить код більш ефективним, організованим та зменшує дублювання

// 2. Виклик super() у конструкторі класу-нащадка є способом викликати конструктор батьківського класу. Це дозволяє ініціалізувати спільні властивості та налаштування з батьківського класу перед тим, як ви додаєте або перевизначаєте властивості в класі-нащадку.


// Клас Employee
class Employee {
    constructor(name, age, salary) {
      this.name = name;
      this.age = age;
      this.salary = salary;
    }
  
    // Гетери та сеттери
    get getName() {
      return this.name;
    }
  
    set setName(name) {
      this.name = name;
    }
  
    get getAge() {
      return this.age;
    }
  
    set setAge(age) {
      this.age = age;
    }
  
    get getSalary() {
      return this.salary;
    }
  
    set setSalary(salary) {
      this.salary = salary;
    }
  }
  
  // Клас Programmer успадковується від Employee
  class Programmer extends Employee {
    constructor(name, age, salary, lang) {
      super(name, age, salary);
      this.lang = lang;
    }
  
    // Перезаписаний гетер для salary
    get getSalary() {
      return this.salary * 3;
    }
  }
  
  // Створення екземплярів класу Programmer
  const programmer1 = new Programmer('John', 30, 50000, ['JavaScript', 'Python']);
  const programmer2 = new Programmer('Alice', 28, 60000, ['Java', 'C++']);
  
  // Виведення інформації про програмістів в консоль
  console.log(programmer1.getName, programmer1.getAge, programmer1.getSalary, programmer1.lang);
  console.log(programmer2.getName, programmer2.getAge, programmer2.getSalary, programmer2.lang);